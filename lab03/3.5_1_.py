import random
import time


def generate_random_data(size):
    return [random.randint(0, 1000) for _ in range(size)]


def generate_worst_case_data(size):
    return list(range(size))


def measure_time(algorithm, A, c):
    start_time = time.time()
    algorithm(A, 0, len(A) - 1, c)
    end_time = time.time()
    return end_time - start_time


def quickSort_modified(A, p, r, c):
    if p >= r:
        return
    if r - p + 1 < c:
        insertionSort(A, p, r)
    else:
        q = partition(A, p, r)
        quickSort_modified(A, p, q, c)
        quickSort_modified(A, q + 1, r, c)


def partition(A, p, r):
    x = A[r]  # element wyznaczający podział
    i = p - 1
    for j in range(p, r + 1):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i - 1


def insertionSort(A, p, r):
    for i in range(p + 1, r + 1):
        key = A[i]
        j = i - 1
        while j >= p and A[j] > key:
            A[j + 1] = A[j]
            j -= 1
        A[j + 1] = key


# Testowanie dla danych losowych
print("Dane losowe:")
data_sizes = [500]  # różne wielkości danych do przetestowania
c = 10  # wartość c

for size in data_sizes:
    A_random = generate_random_data(size)

    time_modified = measure_time(quickSort_modified, A_random.copy(), c)

    print(f"Wielkość danych: {size}")
    print(f"Czas sortowania dla modyfikowanego algorytmu: {time_modified:} sekund")
    print()

# Testowanie dla danych skrajnie niekorzystnych
print("Dane skrajnie niekorzystne (ciąg rosnący):")
for size in data_sizes:
    A_worst_case = generate_worst_case_data(size)

    time_modified = measure_time(quickSort_modified, A_worst_case.copy(), c)

    print(f"Wielkość danych: {size}")
    print(f"Czas sortowania dla modyfikowanego algorytmu: {time_modified:} sekund")
    print()
