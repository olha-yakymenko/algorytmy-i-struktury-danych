def quickSort(A,p,r):
    if p<r :
        q = partition(A,p,r)
        quickSort(A,p,q)
        quickSort(A,q+1,r)

def partition(A,p,r):
    x=A[r] # element wyznaczajacy podział
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
    if i<r :
        return i
    else:
        return i-1

tab=[2,1,2,4,5,3,5,7]
print(quickSort(tab,0,len(tab)-1))
print(tab)


import random
import time


def generate_random_data(size):
    return [random.randint(0, 1000) for _ in range(size)]


def generate_worst_case_data(size):
    return list(range(size))


def measure_time(algorithm, A):
    start_time = time.time()
    algorithm(A, 0, len(A) - 1)
    end_time = time.time()
    return end_time - start_time


# Testowanie dla danych losowych
print("Dane losowe:")
data_sizes = [500]  # różne wielkości danych do przetestowania
c = 10  # wartość c

for size in data_sizes:
    A_random = generate_random_data(size)

    time_modified = measure_time(quickSort, A_random.copy())

    print(f"Wielkość danych: {size}")
    print(f"Czas sortowania dla modyfikowanego algorytmu: {time_modified:} sekund")
    print()

# Testowanie dla danych skrajnie niekorzystnych
print("Dane skrajnie niekorzystne (ciąg rosnący):")
for size in data_sizes:
    A_worst_case = generate_worst_case_data(size)

    time_modified = measure_time(quickSort, A_worst_case.copy())

    print(f"Wielkość danych: {size}")
    print(f"Czas sortowania dla modyfikowanego algorytmu: {time_modified:} sekund")
    print()
import random
import time


def generate_random_data(size):
    return [random.randint(0, 1000) for _ in range(size)]


def generate_worst_case_data(size):
    return list(range(size))


def measure_time(algorithm, A, c):
    start_time = time.time()
    algorithm(A, 0, len(A) - 1, c)
    end_time = time.time()
    return end_time - start_time
